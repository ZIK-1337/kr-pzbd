CREATE TRIGGER before_delete_abonent INSTEAD OF DELETE ON all_abonents
FOR EACH ROW EXECUTE PROCEDURE delete_abonent();