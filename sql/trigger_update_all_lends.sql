CREATE TRIGGER update_all_lends INSTEAD OF UPDATE ON all_lends
FOR EACH ROW EXECUTE PROCEDURE update_all_lends();