CREATE OR REPLACE FUNCTION insert_all_lends() RETURNS TRIGGER AS $$ BEGIN
IF NEW.code NOT IN (SELECT code FROM books_in_library) THEN
RAISE EXCEPTION 'Такой книги сейчас нет в наличии!'; RETURN NULL; END IF;
UPDATE all_books SET count_book = count_book - 1 WHERE code = NEW.code;
INSERT INTO lend(bilet_number, book_code, date_lend, date_return, penny) VALUES
(NEW.bilet_number, NEW.code, NEW.date_lend, NEW.date_return, NEW.penny);
RETURN NEW;
END; $$ LANGUAGE plpgsql;