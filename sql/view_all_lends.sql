CREATE VIEW all_lends AS
SELECT abonents.bilet_number, abonents.last_name, books.code, books.title, lend.date_lend, lend.date_return, lend.penny
FROM abonents INNER JOIN lend ON abonents.bilet_number = lend.bilet_number INNER JOIN books ON lend.book_code = books.code;
