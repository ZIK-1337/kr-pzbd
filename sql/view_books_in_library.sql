CREATE VIEW books_in_library AS
SELECT author, title FROM books WHERE count_book > 0;