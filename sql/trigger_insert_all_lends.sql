CREATE TRIGGER insert_all_lends INSTEAD OF INSERT ON all_lends
FOR EACH ROW EXECUTE PROCEDURE insert_all_lends();