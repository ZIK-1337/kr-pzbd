CREATE OR REPLACE FUNCTION create_user(user_name varchar, role_name varchar)
RETURNS varchar AS 
$BODY$ 
DECLARE pass text;
BEGIN
SELECT random_string((SELECT random_int_between(5,15))) INTO pass;
IF role_name = 'operators' THEN
	EXECUTE format('CREATE USER %I WITH CREATEROLE PASSWORD %L', user_name, pass);
ELSE
	EXECUTE format('CREATE USER %I PASSWORD %L', user_name, pass);
END IF;
EXECUTE format('GRANT %I TO %I', role_name, user_name);
IF role_name = 'unpriv_users' THEN
	INSERT INTO users VALUES(user_name, 0, 0); END IF;
IF role_name = 'priv_users' THEN
	INSERT INTO users VALUES(user_name, 0, 1); END IF;
IF role_name = 'operators' THEN
	INSERT INTO users VALUES(user_name, 0, 2); END IF;
RETURN pass;
END; 
$BODY$ LANGUAGE plpgsql;