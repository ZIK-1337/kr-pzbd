CREATE ROLE priv_users;
GRANT SELECT ON my_info TO priv_users;
GRANT SELECT ON my_lends TO priv_users;
GRANT SELECT ON books_in_library TO priv_users;
GRANT UPDATE (address, phone) ON my_info TO priv_users;