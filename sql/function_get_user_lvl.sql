CREATE OR REPLACE FUNCTION get_user_lvl(user_name varchar)
RETURNS SETOF record  AS 
$BODY$ 
BEGIN
RETURN QUERY SELECT min_lvl, max_lvl FROM users WHERE user_login=user_name;
END; 
$BODY$ LANGUAGE plpgsql;