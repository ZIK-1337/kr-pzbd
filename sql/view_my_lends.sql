CREATE VIEW my_lends AS
SELECT books.title, lend.date_lend, lend.date_return, lend.penny
FROM abonents INNER JOIN lend ON abonents.bilet_number = lend.bilet_number
INNER JOIN books ON lend.book_code = books.code
WHERE (SELECT current_user) = abonents.user_login;