CREATE OR REPLACE FUNCTION update_all_lends() RETURNS TRIGGER AS $$ BEGIN
UPDATE lend SET bilet_number = NEW.bilet_number WHERE id_lend = NEW.id_lend;
IF OLD.code != NEW.code THEN
	IF NEW.code NOT IN (SELECT code FROM books_in_library) THEN
	RAISE EXCEPTION 'Такой книги сейчас нет в наличии!'; RETURN NULL; END IF;
	UPDATE all_books SET count_book = count_book - 1 WHERE code = NEW.code;
	UPDATE all_books SET count_book = count_book + 1 WHERE code = OLD.code;
	UPDATE lend SET book_code = NEW.code WHERE id_lend = NEW.id_lend;
END IF;
UPDATE lend SET date_lend = NEW.date_lend WHERE id_lend = NEW.id_lend;
UPDATE lend SET date_return = NEW.date_return WHERE id_lend = NEW.id_lend;
UPDATE lend SET penny = NEW.penny WHERE id_lend = NEW.id_lend;
RETURN NULL;
END; $$ LANGUAGE plpgsql;