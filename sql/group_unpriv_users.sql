CREATE ROLE unpriv_users;
GRANT SELECT ON my_info TO unpriv_users;
GRANT SELECT ON my_lends TO unpriv_users;