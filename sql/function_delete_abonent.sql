CREATE OR REPLACE FUNCTION delete_abonent() RETURNS TRIGGER AS $$ BEGIN
IF OLD.bilet_number IN (SELECT bilet_number FROM all_lends) THEN
RAISE EXCEPTION 'У этого абонента есть выданные книги!'; RETURN NULL; END IF;
IF OLD.user_login = 'postgres' OR OLD.user_login = 'library_admin' THEN
RAISE EXCEPTION 'Нельзя удалять этих абонентов!'; RETURN NULL; END IF;
DELETE FROM all_users WHERE user_login = OLD.user_login;
EXECUTE format('DROP USER %I', OLD.user_login);
DELETE FROM abonents WHERE bilet_number = OLD.bilet_number;
RETURN NULL;
END; $$ LANGUAGE plpgsql;