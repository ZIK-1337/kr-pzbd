CREATE OR REPLACE FUNCTION delete_all_lends() RETURNS TRIGGER AS $$ BEGIN
UPDATE all_books SET count_book = count_book + 1 WHERE code = OLD.code;
DELETE FROM lend WHERE id_lend = OLD.id_lend;
RETURN NULL;
END; $$ LANGUAGE plpgsql;