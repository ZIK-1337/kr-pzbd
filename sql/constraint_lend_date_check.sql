ALTER TABLE lend ADD CONSTRAINT lend_date_check
CHECK (date_return >= date_lend);