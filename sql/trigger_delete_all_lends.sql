CREATE TRIGGER delete_all_lends INSTEAD OF DELETE ON all_lends
FOR EACH ROW EXECUTE PROCEDURE delete_all_lends();