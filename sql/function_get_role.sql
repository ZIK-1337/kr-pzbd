CREATE FUNCTION get_role(user_name varchar)
RETURNS SETOF name AS 
$BODY$ 
BEGIN
RETURN QUERY
SELECT groname FROM pg_group WHERE (SELECT usesysid FROM pg_user WHERE usename = user_name) = ANY (grolist);
END; 
$BODY$ LANGUAGE plpgsql;