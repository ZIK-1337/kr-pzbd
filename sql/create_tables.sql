CREATE TABLE abonents
(
	bilet_number integer GENERATED ALWAYS AS IDENTITY,
	user_login character varying(50) NOT NULL UNIQUE,
	last_name character varying(20) NOT NULL,
	address character varying(50) NOT NULL,
	phone character varying(15) NOT NULL,
	CONSTRAINT "abonents_pkey_bilet_number" PRIMARY KEY (bilet_number)
);

CREATE TABLE books
(
	code integer GENERATED ALWAYS AS IDENTITY,
	author character varying(50) NOT NULL,
	title character varying(50) NOT NULL,
	count_book integer NOT NULL CHECK (count_book >= 0),
	CONSTRAINT "books_pkey_code" PRIMARY KEY (code)
);

CREATE TABLE lend
(
	id_lend integer GENERATED ALWAYS AS IDENTITY,
	bilet_number integer NOT NULL REFERENCES abonents (bilet_number),
	book_code integer NOT NULL REFERENCES books (code),
	date_lend date NOT NULL,
	date_return date NOT NULL,
	penny integer NOT NULL CHECK (penny >= 0),
	CONSTRAINT "lend_pkey_code" PRIMARY KEY (id_lend)
);

CREATE TABLE users
(
	user_login character varying(50) NOT NULL REFERENCES abonents (user_login),
	min_lvl integer NOT NULL CHECK (min_lvl >= 0),
	max_lvl integer NOT NULL CHECK (max_lvl >= 0),
	CONSTRAINT "users_pkey_user_login" PRIMARY KEY (user_login)
);

CREATE TABLE tables_lvl
(
	t_name character varying(50) NOT NULL,
	lvl integer NOT NULL CHECK (lvl >= 0),
	CONSTRAINT "tables_lvl_pkey_t_name" PRIMARY KEY (t_name)
);