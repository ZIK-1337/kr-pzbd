CREATE OR REPLACE FUNCTION get_table_lvl(name_table varchar)
RETURNS SETOF record  AS 
$BODY$ 
BEGIN
RETURN QUERY SELECT lvl FROM tables_lvl WHERE t_name=name_table;
END; 
$BODY$ LANGUAGE plpgsql;