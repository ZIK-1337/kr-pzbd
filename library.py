import time
from datetime import datetime

import PyQt5.Qt
import psycopg2
import os
import sys
import config
from psycopg2.extras import DictCursor
from psycopg2 import Error
from PyQt5 import QtGui, QtWidgets, QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtSql import QSqlDatabase, QSqlTableModel, QSqlRelationalTableModel, QSql, QSqlRelationalDelegate, \
    QSqlRelation, QSqlQuery
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QMessageBox,
    QTableView,
    QTabWidget,
    QFormLayout,
)
from ui_forms.login_form import Ui_LoginWindow
from ui_forms.main_form import Ui_Form
from ui_forms.logs_form import Ui_Logs


# Функция возвращает connection.
def get_connection(user, password, db):
    try:
        connection = psycopg2.connect(
                                 user=user,
                                 password=password,
                                 dbname=db, cursor_factory=DictCursor)
        post_logs(f"Подключение к БД успешно: юзер - {user}.")
        return connection
    except (Exception, Error) as e:
        if user == config.ADMIN_NAME and str(e):
            QtWidgets.QMessageBox.warning(None, 'Ошибка с БД', str(e),
                                          QtWidgets.QMessageBox.Ok)
        post_logs(f"Подключение к БД неуспешно: юзер - {user}, ошибка - {str(e)}." if str(e)
                  else f"Подключение к БД неуспешно: юзер - {user}.")
        return None


# имя столбцов, которые являются первичным ключом, по имени первичного индекса
def get_pk_from_pi(pi: str, table_name):
    pkeys = pi.replace(table_name + '_pkey_', '')
    return [pkeys]


def post_logs(message):
    with open('logs.txt', mode='a', encoding='utf-8') as f:
        f.write(str(datetime.now()) + ": " + message + '\n')


class NotEditableDelegate(PyQt5.QtWidgets.QStyledItemDelegate):
    def createEditor(self, s1, s2, s3):
        return None

    def initStyleOption(self, option, index):
        super(NotEditableDelegate, self).initStyleOption(option, index)
        option.displayAlignment = QtCore.Qt.AlignCenter


class AlignDelegate(QtWidgets.QStyledItemDelegate):
    def initStyleOption(self, option, index):
        super(AlignDelegate, self).initStyleOption(option, index)
        option.displayAlignment = QtCore.Qt.AlignCenter


class Logs(QMainWindow):
    def __init__(self, user):
        super().__init__()
        self.ui = Ui_Logs()
        self.ui.setupUi(self)
        self.setWindowIcon(QtGui.QIcon('icon.png'))

        self.ui.pushButton.clicked.connect(self.clear)
        if user != config.ADMIN_NAME:
            self.ui.pushButton.setEnabled(False)

    def clear(self):
        result = QMessageBox.question(self, 'Подтверждение',
                                      'Вы действительно хотите очистить логи?',
                                      QMessageBox.Yes, QMessageBox.No)
        if result == QMessageBox.No:
            return
        open('logs.txt', 'w').close()
        self.ui.textBrowser.clear()
        post_logs(f"Очищены логи!")


class Login(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_LoginWindow()
        self.ui.setupUi(self)
        self.setWindowIcon(QtGui.QIcon('icon.png'))
        self.db = QSqlDatabase.addDatabase('QPSQL')
        self.ui.pushButton.clicked.connect(self.login)
        self.forms = []

    # перехват закрытия по крестику
    def closeEvent(self, e):
        result = QMessageBox.question(self, 'Подтверждение', 'Вы действительно хотите выйти?', QMessageBox.Yes,
                                      QMessageBox.No)
        if result == QMessageBox.Yes:
            e.accept()
        else:
            e.ignore()

    def get_lvl(self, user):
        conn = get_connection(config.ADMIN_NAME, config.ADMIN_PASSWORD, config.DB_NAME)
        if conn is None:
            post_logs(f"Не удалось подключиться к БД от имени админа безопасности для получения уровня доступа!")
            return
        with conn.cursor() as cursor:
            query = """SELECT * FROM get_user_lvl(%s) as f(min_lvl integer, max_lvl integer)"""
            cursor.execute(query, (user,))
            min_max_lvl = cursor.fetchall()
        conn.close()
        return min_max_lvl

    def get_tables_for_read_write(self, all_tables, user_lvl, user):
        tables_for_read, tables_for_write = [], []
        conn = get_connection(config.ADMIN_NAME, config.ADMIN_PASSWORD, config.DB_NAME)
        role = ''
        if conn is None:
            post_logs(f"Не удалось подключиться к БД от имени админа безопасности для получения таблиц!")
            return
        with conn.cursor() as cursor:
            if user != 'postgres' and user != 'library_admin':
                query = """SELECT * FROM get_role(%s)"""
                cursor.execute(query, (user,))
                role = cursor.fetchall()[0][0]
            for table in all_tables:
                query = """SELECT * FROM get_table_lvl(%s) as f(lvl integer)"""
                cursor.execute(query, (table,))
                table_lvl = cursor.fetchall()
                if not table_lvl:
                    post_logs(f"У таблицы {table} не задан уровень конфиденциальности!")
                else:
                    if user_lvl >= table_lvl[0][0]:
                        tables_for_read.append(table)
                    if user_lvl == table_lvl[0][0]:
                        if user == 'library_admin' and table in ['all_users', 'all_tables_lvl']:
                            continue
                        if table == 'my_lends':
                            continue
                        if table == 'books_in_library':
                            continue
                        if table == 'my_info' and role == 'unpriv_users':
                            continue
                        tables_for_write.append(table)
        conn.close()
        return tables_for_read, tables_for_write, role

    def login(self):
        user = self.ui.lineEdit.text()
        password = self.ui.lineEdit_2.text()
        if not password or not user:
            QtWidgets.QMessageBox.warning(self, 'Ошибка авторизации',
                                          'Не введен пароль!' if not password
                                          else 'Не введен логин!',
                                          QtWidgets.QMessageBox.Ok)
            return
        conn = get_connection(user, password, config.DB_NAME)
        if conn is None:
            QtWidgets.QMessageBox.warning(self, 'Ошибка авторизации',
                                          'Не удалось подключиться к базе с введенными данными!',
                                          QtWidgets.QMessageBox.Ok)
            post_logs(f"Неудачная попытка входа с данными: логин - {user}, пароль - {password}.")
            return
        max_min_user_lvl = self.get_lvl(user)
        if not max_min_user_lvl:
            QtWidgets.QMessageBox.warning(self, 'Ошибка авторизации',
                                          'У Вас не указан уровень доступа! Обратитесь к администратору безопасности.',
                                          QtWidgets.QMessageBox.Ok)
            post_logs(f"Неудачная попытка входа для логина - {user}. Не указан уровень доступа.")
            conn.close()
            return
        if max_min_user_lvl[0][0] != max_min_user_lvl[0][1]:
            user_lvl = QtWidgets.QInputDialog.getItem(self, 'Уровень доступа.', 'Выберите уровень доступа: ',
                                                      [str(i) for i in range(max_min_user_lvl[0][0],
                                                                             max_min_user_lvl[0][1]+1)], editable=False)
            if not user_lvl[1]:
                QtWidgets.QMessageBox.warning(self, 'Ошибка авторизации', 'Вы отказались от ввода уровня доступа!',
                                              QtWidgets.QMessageBox.Ok)
                post_logs(f"Неудачная попытка входа: пользователь {user} не ввел уровень доступа.")
                return
            form = Form(self.db, user, password, int(user_lvl[0]))
        else:
            form = Form(self.db, user, password, max_min_user_lvl[0][0])
        self.forms.append(form)
        window.hide()
        form.show()
        conn.close()


class Form(QMainWindow):
    def __init__(self, db, user, password, user_lvl):
        super().__init__()
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.setWindowIcon(QtGui.QIcon('icon.png'))
        self.db = db
        self.user = user
        self.password = password
        self.user_lvl = user_lvl
        self.tables_with_fk = {'all_lends': [[1, "abonents", "bilet_number", "bilet_number", "bilet_number"],
                                             [3, "books_in_library", "code", "code", "code"]],
                               'all_users': [[0, "abonents", "user_login", "user_login", "user_login"]]}
        self.forms = []
        self.non_editable_column = ['bilet_number', 'user_login', 'code', 'id_lend']

        post_logs(f'В систему успешно вошел юзер {user} с уровнем доступа {user_lvl}.')
        try:
            self.db.setDatabaseName(config.DB_NAME)
            self.db.setHostName(config.HOST_NAME)
            self.db.setUserName(user)
            self.db.setPassword(password)
            self.db.setPort(config.PORT)
            self.db.open()
        except Exception as e:
            if user == config.ADMIN_NAME and str(e):
                QtWidgets.QMessageBox.warning(None, 'Ошибка с БД', str(e),
                                              QtWidgets.QMessageBox.Ok)
            post_logs(f"Подключение к БД неуспешно: юзер - {user}, ошибка - {str(e)}." if str(e)
                      else f"Подключение к БД неуспешно: юзер - {user}.")
        self.tables = window.get_tables_for_read_write(self.db.tables(QSql.Views), self.user_lvl, self.user)
        for table in self.tables[0]:
            self.fill_tables(table, self.tables[1], self.tables[2])
        self.tab_change()

        self.ui.exit_btn.clicked.connect(self.exit)
        self.ui.pushButton_11.clicked.connect(self.delete)
        self.ui.pushButton_8.clicked.connect(self.update_all)
        self.ui.pushButton_10.clicked.connect(self.update_current)
        self.ui.tabWidget.currentChanged.connect(self.tab_change)
        self.ui.pushButton.clicked.connect(self.insert)
        self.ui.pushButton_2.clicked.connect(self.backup)
        self.ui.pushButton_3.clicked.connect(self.restore)
        self.ui.pushButton_4.clicked.connect(self.get_logs)
        self.ui.find_btn.clicked.connect(self.find_text)

        style = '''
                QTableView::item {background-color: white;
                border: 0.5px outset black;}
                QTableView::item:selected {background-color: blue;
                border: 0.5px inset black; color: white}
                QHeaderView::section { background-color: yellow;
                border: 0.5px outset black;
                font: bold;}
                '''
        self.setStyleSheet(style)

    def find_text(self):
        current_tab = self.ui.tabWidget.currentIndex()
        view = self.ui.tabWidget.widget(current_tab).findChildren(QTableView)[0]
        field = self.ui.find_field.currentIndex()
        text = self.ui.find_text.text()
        founded = view.model().match(view.model().index(0, field), QtCore.Qt.DisplayRole, text, -1,
                                     QtCore.Qt.MatchExactly)
        view.clearSelection()
        view.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        for index in founded:
            view.selectRow(index.row())
        view.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        if not founded:
            QtWidgets.QMessageBox.information(self, 'Поиск', 'Ничего не найдено!')
        else:
            QtWidgets.QMessageBox.information(self, 'Поиск', 'Найденные строки выделены!')

    def get_logs(self):
        form = Logs(self.user)
        self.forms.append(form)
        with open('logs.txt', mode='r', encoding='utf-8') as f:
            form.ui.textBrowser.append('\n'.join(f.readlines()))
        form.show()

    def update_table(self):
        self.update_current()

    def backup(self):
        filename, _filter = QtWidgets.QFileDialog.getSaveFileName(self, 'Выбрать файл для бэкапа', '', 'dump (*.dump)')
        if filename != '':
            os.system(f'pg_dumpall -U {config.ADMIN_NAME} -c > {filename}')
            QtWidgets.QMessageBox.information(self, 'Резервирование', 'Успешно!')
            post_logs(f'Создана резервная копия {filename}.')

    def restore(self):
        filename, _filter = QtWidgets.QFileDialog.getOpenFileName(self, 'Открыть файл с копией', '', 'dump (*.dump)')
        if filename != '':
            os.system(f'psql -o logs.txt -q -U {config.ADMIN_NAME} -f {filename} {config.ADMIN_NAME}')
            os.system('del logs.txt')
            os.system('cls')
            self.update_all()
            QtWidgets.QMessageBox.information(self, 'Восстановление', 'Успешно!')
            post_logs(f'Восстановление БД из файла {filename}.')

    def exit(self):
        self.close()

    def tab_change(self):
        current_tab = self.ui.tabWidget.currentIndex()
        table_name = self.ui.tabWidget.tabText(current_tab)
        try:
            view = self.ui.tabWidget.widget(current_tab).findChildren(QTableView)[0]
        except Exception:
            return
        self.ui.find_field.clear()
        self.ui.find_text.clear()
        for i in range(view.model().columnCount()):
            self.ui.find_field.addItem(view.model().headerData(i, QtCore.Qt.Horizontal, QtCore.Qt.DisplayRole))
        if table_name not in self.tables[1] or table_name in ['my_info']:
            self.ui.pushButton_11.setEnabled(False)
            self.ui.pushButton.setEnabled(False)
        else:
            self.ui.pushButton_11.setEnabled(True)
            self.ui.pushButton.setEnabled(True)
        if self.user == config.ADMIN_NAME:
            self.ui.pushButton_2.setEnabled(True)
            self.ui.pushButton_3.setEnabled(True)
            self.ui.pushButton_4.setEnabled(True)
        else:
            self.ui.pushButton_2.setEnabled(False)
            self.ui.pushButton_3.setEnabled(False)
            self.ui.pushButton_4.setEnabled(False)

    def update_current(self):
        current_tab = self.ui.tabWidget.currentIndex()
        current_name = self.ui.tabWidget.tabText(current_tab)
        self.ui.tabWidget.removeTab(current_tab)
        self.fill_tables(current_name, self.tables[1], self.tables[2])
        self.ui.tabWidget.setCurrentIndex(self.ui.tabWidget.count() - 1)

    def update_all(self):
        # current_tab = self.ui.tabWidget.currentIndex()
        self.ui.tabWidget.clear()
        for table in self.tables[0]:
            self.fill_tables(table, self.tables[1], self.tables[2])
        # self.ui.tabWidget.setCurrentIndex(current_tab)

    def insert(self):
        current_tab = self.ui.tabWidget.currentIndex()
        current_name = self.ui.tabWidget.tabText(current_tab)

        if current_name == 'all_abonents':
            user_role = QtWidgets.QInputDialog.getItem(self, 'Роль абонента.', 'Выберите роль добавляемого абонента: ',
                                                       ['unpriv_users', 'priv_users', 'operators'], editable=False)
            if not user_role[1]:
                QtWidgets.QMessageBox.warning(self, 'Ошибка добавления', 'Вы отказались от ввода роли!',
                                              QtWidgets.QMessageBox.Ok)
                post_logs(f"Неудачная попытка добавления абонента: пользователь {self.user} не ввел роль абонента.")
                return

        gbox = self.ui.tabWidget.widget(current_tab).findChildren(QtWidgets.QGroupBox)
        model1 = QSqlTableModel()
        model1.setTable(current_name)
        model1.select()
        columns = []
        types_columns = []
        for i in range(model1.columnCount()):
            column_name = model1.record().fieldName(i)
            if b'int' in QtCore.QMetaType(model1.record().field(i).type()).name():
                types_columns.append('int')
            elif b'QDate' in QtCore.QMetaType(model1.record().field(i).type()).name():
                types_columns.append('date')
            else:
                types_columns.append('string')
            columns.append(column_name)
        record = model1.record()
        for index, name in enumerate(columns):
            if current_name == 'all_lends' and name in ['id_lend', 'last_name', 'title']:
                record.remove(record.indexOf(name))
                continue
            if current_name == 'all_abonents' and name == 'bilet_number':
                record.remove(record.indexOf(name))
                continue
            if current_name == 'all_books' and name == 'code':
                record.remove(record.indexOf(name))
                continue
            if types_columns[index] == 'int':
                if gbox[0].findChildren(QtWidgets.QSpinBox, f'spin_{name}'):
                    value = gbox[0].findChildren(QtWidgets.QSpinBox, f'spin_{name}')[0].value()
                else:
                    value = gbox[0].findChildren(QtWidgets.QComboBox, f'combo_{name}')[0].currentText()
            elif types_columns[index] == 'date':
                value = gbox[0].findChildren(QtWidgets.QDateEdit, f'date_{name}')[0].date()
            else:
                if gbox[0].findChildren(QtWidgets.QLineEdit, f'edit_{name}'):
                    value = gbox[0].findChildren(QtWidgets.QLineEdit, f'edit_{name}')[0].text()
                else:
                    value = gbox[0].findChildren(QtWidgets.QComboBox, f'combo_{name}')[0].currentText()
            record.setValue(name, value if value or value == 0 else None)

        rec_str = "\n".join([str(record.fieldName(i)) + ": " + str(record.value(i)) for i in range(record.count())])
        result = QMessageBox.question(self, 'Подтверждение',
                                      f'Вы действительно хотите добавить следующую запись?\n{rec_str}',
                                      QMessageBox.Yes, QMessageBox.No)
        if result == QMessageBox.No:
            return

        if not model1.insertRecord(-1, record):
            QtWidgets.QMessageBox.warning(self, 'Вставка', f'Не удалось вставить строку!\n{model1.lastError().text()}')
            post_logs(f'Не удалось вставить в таблицу {current_name} юзеру {self.user} запись:\n{rec_str}\nОшибка:'
                      f'{model1.lastError().text()}')
            return
        self.update_current()
        QtWidgets.QMessageBox.information(self, 'Вставка', 'Запись успешно добавлена!')
        post_logs(f'В таблицу {current_name} юзером {self.user} добавлена запись:\n{rec_str}')
        if current_name == 'all_abonents':
            conn = get_connection(config.ADMIN_NAME, config.ADMIN_PASSWORD, config.DB_NAME)
            if conn is None:
                post_logs(f"Не удалось подключиться к БД от имени админа безопасности для создания роли!")
                return
            with conn.cursor() as cursor:
                query = """SELECT * FROM create_user(%s, %s)"""
                cursor.execute(query, (record.value("user_login"), user_role[0]))
                password = cursor.fetchall()[0][0]
            conn.commit()
            conn.close()
            QtWidgets.QMessageBox.information(self, 'Добавление абонента', f'Высланный пароль - {password}')
            post_logs(f"Пароль нового юзера {record.value('user_login')} - {password}")

    def delete(self):
        current_tab = self.ui.tabWidget.currentIndex()
        current_name = self.ui.tabWidget.tabText(current_tab)
        view = self.ui.tabWidget.widget(current_tab).findChildren(QTableView)[0]
        row_indexes = [index.row() for index in view.selectionModel().selectedRows()]

        if not row_indexes:
            QtWidgets.QMessageBox.warning(self, 'Предупреждение', f'Не выбраны строки для удаления!')
            return
        result = QMessageBox.question(self, 'Подтверждение',
                                      f'Вы действительно хотите удалить запись с индексом/ами = '
                                      f'{", ".join([str(int(index) + 1) for index in row_indexes])}?',
                                      QMessageBox.Yes, QMessageBox.No)
        if result == QMessageBox.No:
            return

        row_indexes.sort(reverse=True)
        model1 = QSqlTableModel()
        model1.setTable(current_name)
        model1.select()
        last_row_count = model1.rowCount()
        not_deleted = []
        for index in row_indexes:
            try:
                view.model().removeRows(index, 1)
            except Exception as e:
                if self.user == config.ADMIN_NAME and str(e):
                    QtWidgets.QMessageBox.warning(self, 'Ошибка при удалении', str(e),
                                                  QtWidgets.QMessageBox.Ok)
                else:
                    QtWidgets.QMessageBox.warning(self, 'Удаление', f'Не удалось удалить строку {str(index)}!')
                post_logs(f"Не удалось удалить строку {str(index)} из таблицы {current_name}: "
                          f"юзер - {self.user}, ошибка - {str(e)}." if str(e)
                          else f"Не удалось удалить строку {str(index)} из таблицы {current_name}: юзер - {self.user}.")
            model1.select()
            if last_row_count == model1.rowCount():
                QtWidgets.QMessageBox.warning(self, 'Удаление', f'Не удалена запись с индексом {str(index+1)} - '
                                              f'есть ссылки на эту запись в других таблицах. Либо вы пытаетесь '
                                                                f'удалить себя или администратора.')
                not_deleted.append(index)
            else:
                last_row_count = model1.rowCount()
        self.update_current()
        deleted = []
        for index in row_indexes:
            if index not in not_deleted:
                deleted.append(str(index + 1))
        if deleted:
            QtWidgets.QMessageBox.information(self, 'Удаление', f'Записи с индексами {", ".join(deleted)} успешно '
                                                                f'удалены!')
            post_logs(f"Произошло удаление строк/и {', '.join(deleted)} "
                      f"из таблицы {current_name}: юзер - {self.user}.")

    def fill_tables(self, table_name, table_for_write, role):
        if table_name not in self.tables_with_fk:
            model = QSqlTableModel(self)
            model.setEditStrategy(QSqlTableModel.OnFieldChange)
            model.setTable(table_name)
            model.select()
        else:
            model = QSqlRelationalTableModel(self)
            model.setEditStrategy(QSqlRelationalTableModel.OnFieldChange)
            model.setTable(table_name)
            model.select()
            for fkey in range(len(self.tables_with_fk[table_name])):
                model.setRelation(self.tables_with_fk[table_name][fkey][0],
                                  QSqlRelation(self.tables_with_fk[table_name][fkey][1],
                                               self.tables_with_fk[table_name][fkey][2],
                                               self.tables_with_fk[table_name][fkey][3]))
        p_keys = get_pk_from_pi(model.primaryKey().name(), table_name)
        pk_index = []
        font = QtGui.QFont()
        font.setPointSize(12)
        columns = []
        types_columns = []
        for i in range(model.columnCount()):
            column_name = model.record().fieldName(i)
            if b'int' in QtCore.QMetaType(model.record().field(i).type()).name():
                types_columns.append('int')
            elif b'QDate' in QtCore.QMetaType(model.record().field(i).type()).name():
                types_columns.append('date')
            else:
                types_columns.append('string')
            columns.append(column_name)
            model.setHeaderData(i, Qt.Horizontal, column_name)
            if column_name in p_keys or column_name in self.non_editable_column:
                pk_index.append(i)
                continue
            if table_name == 'all_lends' and column_name in ['last_name', 'title']:
                pk_index.append(i)
                continue
            if table_name == 'my_info' and role == 'priv_users' and column_name == 'last_name':
                pk_index.append(i)
                continue
        tab = QTabWidget()
        tab.setFont(font)
        self.ui.gridLayout.addWidget(tab)
        tab_number = self.ui.tabWidget.addTab(tab, table_name)
        view = QTableView(tab)
        form_layout = QFormLayout(tab)
        form_layout.addWidget(view)
        view.setFont(font)
        view.horizontalHeader().setSectionResizeMode(PyQt5.Qt.QHeaderView.Stretch)
        view.horizontalHeader().setFont(font)
        view.verticalHeader().setFont(font)
        if table_name not in table_for_write:
            view.setEditTriggers(PyQt5.Qt.QAbstractItemView.NoEditTriggers)
        elif table_name in ['my_info']:
            pass
        else:
            box = QtWidgets.QGroupBox(tab)
            box.setTitle('Добавление записи')
            box.setFont(font)
            form_layout.addWidget(box)
            box_layout = QtWidgets.QFormLayout(box)
            for index, column in enumerate(columns):
                if table_name == 'all_lends' and column in ['id_lend', 'last_name', 'title']:
                    continue
                if table_name == 'all_abonents' and column == 'bilet_number':
                    continue
                if table_name == 'all_books' and column == 'code':
                    continue
                label = QtWidgets.QLabel(box)
                label.setFont(font)
                label.setObjectName(f'label_{column}')
                label.setText(column)
                box_layout.setWidget(box_layout.rowCount(), QtWidgets.QFormLayout.LabelRole, label)
                if table_name not in self.tables_with_fk:
                    if types_columns[index] == 'string':
                        edit = QtWidgets.QLineEdit(box)
                        edit.setFont(font)
                        edit.setObjectName(f'edit_{column}')
                        box_layout.setWidget(box_layout.rowCount() - 1, QtWidgets.QFormLayout.FieldRole, edit)
                    elif types_columns[index] == 'date':
                        date_edit = QtWidgets.QDateEdit(box)
                        date_edit.setFont(font)
                        date_edit.setObjectName(f'date_{column}')
                        date_edit.setCalendarPopup(True)
                        date_edit.setDate(QtCore.QDate(datetime.now().year, datetime.now().month, datetime.now().day))
                        box_layout.setWidget(box_layout.rowCount() - 1, QtWidgets.QFormLayout.FieldRole, date_edit)
                    else:
                        spin = QtWidgets.QSpinBox(box)
                        spin.setFont(font)
                        spin.setObjectName(f'spin_{column}')
                        spin.setMaximum(100000)
                        box_layout.setWidget(box_layout.rowCount() - 1, QtWidgets.QFormLayout.FieldRole, spin)
                else:
                    added = False
                    for fkey in range(len(self.tables_with_fk[table_name])):
                        if column == self.tables_with_fk[table_name][fkey][4]:
                            combo = QtWidgets.QComboBox(box)
                            combo.setFont(font)
                            combo.setObjectName(f'combo_{column}')
                            model1 = QSqlTableModel()
                            model1.setTable(self.tables_with_fk[table_name][fkey][1])
                            model1.select()
                            for i in range(model1.rowCount()):
                                combo.addItem(str(model1.record(i).value(0) if table_name != "all_users" else
                                                  model1.record(i).value(1)))
                            box_layout.setWidget(box_layout.rowCount() - 1, QtWidgets.QFormLayout.FieldRole, combo)
                            added = True
                            break
                    if not added:
                        if types_columns[index] == 'string':
                            edit = QtWidgets.QLineEdit(box)
                            edit.setFont(font)
                            edit.setObjectName(f'edit_{column}')
                            box_layout.setWidget(box_layout.rowCount() - 1, QtWidgets.QFormLayout.FieldRole, edit)
                        elif types_columns[index] == 'date':
                            date_edit = QtWidgets.QDateEdit(box)
                            date_edit.setFont(font)
                            date_edit.setObjectName(f'date_{column}')
                            date_edit.setCalendarPopup(True)
                            date_edit.setDate(
                                QtCore.QDate(datetime.now().year, datetime.now().month, datetime.now().day))
                            box_layout.setWidget(box_layout.rowCount() - 1, QtWidgets.QFormLayout.FieldRole, date_edit)
                        else:
                            spin = QtWidgets.QSpinBox(box)
                            spin.setFont(font)
                            spin.setObjectName(f'spin_{column}')
                            spin.setMaximum(100000)
                            box_layout.setWidget(box_layout.rowCount() - 1, QtWidgets.QFormLayout.FieldRole, spin)
        view.setModel(model)
        for i in pk_index:
            view.setItemDelegateForColumn(i, NotEditableDelegate(self))
        view.setItemDelegate(AlignDelegate(self))
        if table_name in self.tables_with_fk and table_name in table_for_write:
            for fkey in range(len(self.tables_with_fk[table_name])):
                if table_name == 'all_users':
                    continue
                view.setItemDelegateForColumn(self.tables_with_fk[table_name][fkey][0], QSqlRelationalDelegate(self))
        model.dataChanged.connect(self.update_table)

    # перехват закрытия по крестику
    def closeEvent(self, e):
        result = QMessageBox.question(self, 'Подтверждение', 'Вы действительно хотите выйти?',
                                      QMessageBox.Yes, QMessageBox.No)
        if result == QMessageBox.Yes:
            e.accept()
            if self.db.isOpen():
                self.db.close()
            window.show()
        else:
            e.ignore()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    splash_pix = QtGui.QPixmap('splash.jpg')
    splash = QtWidgets.QSplashScreen(splash_pix, QtCore.Qt.WindowStaysOnTopHint)
    splash.show()
    time.sleep(2)
    splash.close()
    window = Login()
    window.show()
    app.exec_()
